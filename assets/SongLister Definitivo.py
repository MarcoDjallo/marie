import os

# percorso completo della cartella base in cui ci sono i file
# possono anche essere suddivisi in ulteriori cartelle annidate
CARTELLA_SUL_PC = 'C:\\DATA\\m_portable\\PortableGit\\marie\\assets\\howler.js-2.2.3\\examples\\player\\audio'

# percorso nel quale verranno caricate le canzoni sul server web
# relativo alla web root o alla cartella in cui andra il file index.html
CARTELLA_SUL_SERVER_WEB = 'C:\\DATA\\m_portable\\PortableGit\\marie\\assets\\howler.js-2.2.3\\examples\\player\\audio\\'

# lista di estensioni dei file da considerare
ESTENSIONI = ('.mp3')

# file in cui viene scritta la lista di output
# (fare copia/incolla da qui https://github.com/goldfire/howler.js/blob/143ae442386c7b42d91a007d0b1f1695528abe64/examples/player/player.js#L274)
OUTPUT_FILE = 'list.txt'

lines = []
for dirpath, currentDirectory, files in os.walk(CARTELLA_SUL_PC):
    for file in files:
        if file.endswith(ESTENSIONI):
            # qui viene scelto il titolo dal nome del file
            # per ora, è il nome file meno l'estensione
            title = ''.join(file.split('.')[:-1])
            titrep = title.replace("'","@'")
            filepath = os.path.join(dirpath, file)
            filepathrep = filepath.replace("'","@'")
            line = "{ title: '%s', file: '%s', howl: null }" % (titrep, filepathrep)
            lines.append(line)


with open(OUTPUT_FILE, 'w', encoding="utf-8") as out_file:
    out_file.write(','.join(lines))
