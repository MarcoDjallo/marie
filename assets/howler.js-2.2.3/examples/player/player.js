/*!
 *  Howler.js Audio Player Demo
 *  howlerjs.com
 *
 *  (c) 2013-2020, James Simpson of GoldFire Studios
 *  goldfirestudios.com
 *
 *  MIT License
 */

// Cache references to DOM elements.
var elms = ['track', 'timer', 'duration', 'playBtn', 'pauseBtn', 'prevBtn', 'nextBtn', 'playlistBtn', 'volumeBtn', 'progress', 'bar', 'wave', 'loading', 'playlist', 'list', 'volume', 'barEmpty', 'barFull', 'sliderBtn'];
elms.forEach(function(elm) {
  window[elm] = document.getElementById(elm);
});

/**
 * Player class containing the state of our playlist and where we are in it.
 * Includes all methods for playing, skipping, updating the display, etc.
 * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
 */
var Player = function(playlist) {
  this.playlist = playlist;
  this.index = 0;

  // Display the title of the first track.
  track.innerHTML = '1. ' + playlist[0].title;

  // Setup the playlist display.
  playlist.forEach(function(song) {
    var div = document.createElement('div');
    div.className = 'list-song';
    div.innerHTML = song.title;
    div.onclick = function() {
      player.skipTo(playlist.indexOf(song));
    };
    list.appendChild(div);
  });
};
Player.prototype = {
  /**
   * Play a song in the playlist.
   * @param  {Number} index Index of the song in the playlist (leave empty to play the first or current).
   */
  play: function(index) {
    var self = this;
    var sound;

    index = typeof index === 'number' ? index : self.index;
    var data = self.playlist[index];

    // If we already loaded this track, use the current one.
    // Otherwise, setup and load a new Howl.
    if (data.howl) {
      sound = data.howl;
    } else {
      sound = data.howl = new Howl({
        src: ['./audio/' + data.file + '.webm', './audio/' + data.file + '.mp3'],
        html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
        onplay: function() {
          // Display the duration.
          duration.innerHTML = self.formatTime(Math.round(sound.duration()));

          // Start updating the progress of the track.
          requestAnimationFrame(self.step.bind(self));

          // Start the wave animation if we have already loaded
          wave.container.style.display = 'block';
          bar.style.display = 'none';
          pauseBtn.style.display = 'block';
        },
        onload: function() {
          // Start the wave animation.
          wave.container.style.display = 'block';
          bar.style.display = 'none';
          loading.style.display = 'none';
        },
        onend: function() {
          // Stop the wave animation.
          wave.container.style.display = 'none';
          bar.style.display = 'block';
          self.skip('next');
        },
        onpause: function() {
          // Stop the wave animation.
          wave.container.style.display = 'none';
          bar.style.display = 'block';
        },
        onstop: function() {
          // Stop the wave animation.
          wave.container.style.display = 'none';
          bar.style.display = 'block';
        },
        onseek: function() {
          // Start updating the progress of the track.
          requestAnimationFrame(self.step.bind(self));
        }
      });
    }

    // Begin playing the sound.
    sound.play();

    // Update the track display.
    track.innerHTML = (index + 1) + '. ' + data.title;

    // Show the pause button.
    if (sound.state() === 'loaded') {
      playBtn.style.display = 'none';
      pauseBtn.style.display = 'block';
    } else {
      loading.style.display = 'block';
      playBtn.style.display = 'none';
      pauseBtn.style.display = 'none';
    }

    // Keep track of the index we are currently playing.
    self.index = index;
  },

  /**
   * Pause the currently playing track.
   */
  pause: function() {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Puase the sound.
    sound.pause();

    // Show the play button.
    playBtn.style.display = 'block';
    pauseBtn.style.display = 'none';
  },

  /**
   * Skip to the next or previous track.
   * @param  {String} direction 'next' or 'prev'.
   */
  skip: function(direction) {
    var self = this;

    // Get the next track based on the direction of the track.
    var index = 0;
    if (direction === 'prev') {
      index = self.index - 1;
      if (index < 0) {
        index = self.playlist.length - 1;
      }
    } else {
      index = self.index + 1;
      if (index >= self.playlist.length) {
        index = 0;
      }
    }

    self.skipTo(index);
  },

  /**
   * Skip to a specific track based on its playlist index.
   * @param  {Number} index Index in the playlist.
   */
  skipTo: function(index) {
    var self = this;

    // Stop the current track.
    if (self.playlist[self.index].howl) {
      self.playlist[self.index].howl.stop();
    }

    // Reset progress.
    progress.style.width = '0%';

    // Play the new track.
    self.play(index);
  },

  /**
   * Set the volume and update the volume slider display.
   * @param  {Number} val Volume between 0 and 1.
   */
  volume: function(val) {
    var self = this;

    // Update the global volume (affecting all Howls).
    Howler.volume(val);

    // Update the display on the slider.
    var barWidth = (val * 90) / 100;
    barFull.style.width = (barWidth * 100) + '%';
    sliderBtn.style.left = (window.innerWidth * barWidth + window.innerWidth * 0.05 - 25) + 'px';
  },

  /**
   * Seek to a new position in the currently playing track.
   * @param  {Number} per Percentage through the song to skip.
   */
  seek: function(per) {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Convert the percent into a seek position.
    if (sound.playing()) {
      sound.seek(sound.duration() * per);
    }
  },

  /**
   * The step called within requestAnimationFrame to update the playback position.
   */
  step: function() {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Determine our current seek position.
    var seek = sound.seek() || 0;
    timer.innerHTML = self.formatTime(Math.round(seek));
    progress.style.width = (((seek / sound.duration()) * 100) || 0) + '%';

    // If the sound is still playing, continue stepping.
    if (sound.playing()) {
      requestAnimationFrame(self.step.bind(self));
    }
  },

  /**
   * Toggle the playlist display on/off.
   */
  togglePlaylist: function() {
    var self = this;
    var display = (playlist.style.display === 'block') ? 'none' : 'block';

    setTimeout(function() {
      playlist.style.display = display;
    }, (display === 'block') ? 0 : 500);
    playlist.className = (display === 'block') ? 'fadein' : 'fadeout';
  },

  /**
   * Toggle the volume display on/off.
   */
  toggleVolume: function() {
    var self = this;
    var display = (volume.style.display === 'block') ? 'none' : 'block';

    setTimeout(function() {
      volume.style.display = display;
    }, (display === 'block') ? 0 : 500);
    volume.className = (display === 'block') ? 'fadein' : 'fadeout';
  },

  /**
   * Format the time from seconds to M:SS.
   * @param  {Number} secs Seconds to format.
   * @return {String}      Formatted time.
   */
  formatTime: function(secs) {
    var minutes = Math.floor(secs / 60) || 0;
    var seconds = (secs - minutes * 60) || 0;

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
  }
};

// Setup our new audio player class and pass it the playlist.
var player = new Player([
{ title: 'Alexia Avina _ Betting On An Island _ 1 - I Don\'t Want All Your Money', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 1 - I Don\'t Want All Your Money', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 2 - Song 36', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 2 - Song 36', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 3 - Glove', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 3 - Glove', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 4 - Bad', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 4 - Bad', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 5 - Betting On An Island', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 5 - Betting On An Island', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 6 - Bird', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 6 - Bird', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 7 - X', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 7 - X', howl: null },{ title: 'Alexia Avina _ Betting On An Island _ 8 - Don\'t You Give', file: '/Alexia Avina/Betting On An Island/Alexia Avina _ Betting On An Island _ 8 - Don\'t You Give', howl: null },{ title: 'Alexia Avina _ Kind Forest EP _ 1 - (bored) Summer', file: '/Alexia Avina/Kind Forest EP/Alexia Avina _ Kind Forest EP _ 1 - (bored) Summer', howl: null },{ title: 'Alexia Avina _ Kind Forest EP _ 2 - Rain + Boy', file: '/Alexia Avina/Kind Forest EP/Alexia Avina _ Kind Forest EP _ 2 - Rain + Boy', howl: null },{ title: 'Alexia Avina _ Kind Forest EP _ 3 - Walk Home', file: '/Alexia Avina/Kind Forest EP/Alexia Avina _ Kind Forest EP _ 3 - Walk Home', howl: null },{ title: 'Alexia Avina _ Kind Forest EP _ 4 - Bedrooms', file: '/Alexia Avina/Kind Forest EP/Alexia Avina _ Kind Forest EP _ 4 - Bedrooms', howl: null },{ title: 'Alexia Avina _ Unearth _ 1 - Cups', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 1 - Cups', howl: null },{ title: 'Alexia Avina _ Unearth _ 10 - Synth Jam', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 10 - Synth Jam', howl: null },{ title: 'Alexia Avina _ Unearth _ 11 - Night Sky', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 11 - Night Sky', howl: null },{ title: 'Alexia Avina _ Unearth _ 2 - Inner Garden', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 2 - Inner Garden', howl: null },{ title: 'Alexia Avina _ Unearth _ 3 - Horse\'s Mane', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 3 - Horse\'s Mane', howl: null },{ title: 'Alexia Avina _ Unearth _ 4 - Fit Into', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 4 - Fit Into', howl: null },{ title: 'Alexia Avina _ Unearth _ 5 - I Love Watching You Live', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 5 - I Love Watching You Live', howl: null },{ title: 'Alexia Avina _ Unearth _ 6 - Walk a Line', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 6 - Walk a Line', howl: null },{ title: 'Alexia Avina _ Unearth _ 7 - I Wouldn\'t Go', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 7 - I Wouldn\'t Go', howl: null },{ title: 'Alexia Avina _ Unearth _ 8 - Falling Starts', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 8 - Falling Starts', howl: null },{ title: 'Alexia Avina _ Unearth _ 9 - Feeding That Beast', file: '/Alexia Avina/Unearth/Alexia Avina _ Unearth _ 9 - Feeding That Beast', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 1 - Fake Plastic Trees', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 1 - Fake Plastic Trees', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 2 - High And Dry', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 2 - High And Dry', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 3 - No Surprises', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 3 - No Surprises', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 4 - Idioteque', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 4 - Idioteque', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 5 - Creep (Hungover at Soundcheck in Berlin)', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 5 - Creep (Hungover at Soundcheck in Berlin)', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 6 - Exit Music (For A Film)', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 6 - Exit Music (For A Film)', howl: null },{ title: 'Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 7 - Creep (Live in Prague)', file: '/Amanda Palmer/Radiohead/Amanda Palmer _ Amanda Palmer Performs The Popular Hits Of Radiohead On Her Magical Ukulele _ 7 - Creep (Live in Prague)', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 1 - Earth People Fair', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 1 - Earth People Fair', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 10 - Not the End', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 10 - Not the End', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 11 - Trees Eat Us All', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 11 - Trees Eat Us All', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 12 - Spaceship Earth', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 12 - Spaceship Earth', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 2 - Grow a Garden', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 2 - Grow a Garden', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 3 - Dad\'s Dunny', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 3 - Dad\'s Dunny', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 4 - Plant Some Trees', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 4 - Plant Some Trees', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 5 - Human', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 5 - Human', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 6 - Everybody\'s Crazy', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 6 - Everybody\'s Crazy', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 7 - Frustration', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 7 - Frustration', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 8 - Singing Makes It Better', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 8 - Singing Makes It Better', howl: null },{ title: 'Formidable Vegetable _ Earth People Fair _ 9 - Pick Me up - Hitchhiking Song', file: '/Formidable Vegetable/Formidable Vegetable - Earth People Fair (2019)/Formidable Vegetable _ Earth People Fair _ 9 - Pick Me up - Hitchhiking Song', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 1 - Life Is Chaos', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 1 - Life Is Chaos', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 2 - Angel Bridge', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 2 - Angel Bridge', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 3 - Snow Leopard', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 3 - Snow Leopard', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 4 - Painkiller Weekend', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 4 - Painkiller Weekend', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 5 - The Calm', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 5 - The Calm', howl: null },{ title: 'Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 6 - Toys in the Sand', file: '/Jono & The Uke Dealers/Jono & The Uke Dealers _ Jono & The Uke Dealers EP _ 6 - Toys in the Sand', howl: null },{ title: 'Louie Zong _ souvenirs _ 1 - lochs and keys', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 1 - lochs and keys', howl: null },{ title: 'Louie Zong _ souvenirs _ 2 - twirling dresses', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 2 - twirling dresses', howl: null },{ title: 'Louie Zong _ souvenirs _ 3 - unearthly light', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 3 - unearthly light', howl: null },{ title: 'Louie Zong _ souvenirs _ 4 - cowbells', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 4 - cowbells', howl: null },{ title: 'Louie Zong _ souvenirs _ 5 - clubs and catacombs', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 5 - clubs and catacombs', howl: null },{ title: 'Louie Zong _ souvenirs _ 6 - return flight', file: '/Louie Zong/souvenirs/Louie Zong _ souvenirs _ 6 - return flight', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 10XSOP - A Miniature Finale', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 10XSOP - A Miniature Finale', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 1XSOP - My Petit Prelude', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 1XSOP - My Petit Prelude', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 2XSOP - Yesterday', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 2XSOP - Yesterday', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 3XSOP - Wake Up Wake Up', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 3XSOP - Wake Up Wake Up', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 4XSOP - Leaves Falling', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 4XSOP - Leaves Falling', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 5XSOP - Puddles on the Playground', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 5XSOP - Puddles on the Playground', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 6XSOP - Morning Coffee', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 6XSOP - Morning Coffee', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 7XSOP - Afternoon Nap (For Pets)', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 7XSOP - Afternoon Nap (For Pets)', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 8XSOP - Pitter-Patter Interlude', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 8XSOP - Pitter-Patter Interlude', howl: null },{ title: 'Lullatone _ Little Songs About Raindrops _ 9XSOP - Drip Drops Jumping on an Umbrella', file: '/Lullatone/Little Songs About Raindrops/Lullatone _ Little Songs About Raindrops _ 9XSOP - Drip Drops Jumping on an Umbrella', howl: null },{ title: 'Lullatone - a picture of your grandparents when they were young', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - a picture of your grandparents when they were young', howl: null },{ title: 'Lullatone - a runaway kite', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - a runaway kite', howl: null },{ title: 'Lullatone - an inherited record collection', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - an inherited record collection', howl: null },{ title: 'Lullatone - an older couple holding hands', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - an older couple holding hands', howl: null },{ title: 'Lullatone - brass practice', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - brass practice', howl: null },{ title: 'Lullatone - checking things off of a to-do list early in the morning', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - checking things off of a to-do list early in the morning', howl: null },{ title: 'Lullatone - clapping contest', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - clapping contest', howl: null },{ title: 'Lullatone - finding a leaf in your girlfriend\'s hair', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - finding a leaf in your girlfriend\'s hair', howl: null },{ title: 'Lullatone - going to buy some strawberries', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - going to buy some strawberries', howl: null },{ title: 'Lullatone - growing up', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - growing up', howl: null },{ title: 'Lullatone - listening to raindrops knocking on a window', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - listening to raindrops knocking on a window', howl: null },{ title: 'Lullatone - little things swimming under a microscope', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - little things swimming under a microscope', howl: null },{ title: 'Lullatone - riding a bike down a big hill and taking your feet off of the pedals', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - riding a bike down a big hill and taking your feet off of the pedals', howl: null },{ title: 'Lullatone - the best paper airplane ever', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - the best paper airplane ever', howl: null },{ title: 'Lullatone - the kind of song you make up in your head when you are bored', file: '/Lullatone/Lullatone - Soundtracks For Everyday Adventures/Lullatone - the kind of song you make up in your head when you are bored', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 01 - A Natural Alarm Clock', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 01 - A Natural Alarm Clock', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 02 - Waking Up on a Picnic Blanket', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 02 - Waking Up on a Picnic Blanket', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 03 - Gathering Sticks for a Nest', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 03 - Gathering Sticks for a Nest', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 04 - Origami Tulips', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 04 - Origami Tulips', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 05 - Outside Sandwiches', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 05 - Outside Sandwiches', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 06 - Perfectly Organized Cleaning Supplies', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 06 - Perfectly Organized Cleaning Supplies', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 07 - The Very First Palm Tree of Spring Break', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 07 - The Very First Palm Tree of Spring Break', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 08 - A Sky Full of Hot Air Balloons', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 08 - A Sky Full of Hot Air Balloons', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 09 - Sprouts in the Cracks in the Concrete', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 09 - Sprouts in the Cracks in the Concrete', howl: null },{ title: 'Lullatone _ The Sounds of Spring EP _ 10 - Wet Grass', file: '/Lullatone/The Sounds of Spring EP/Lullatone _ The Sounds of Spring EP _ 10 - Wet Grass', howl: null },{ title: 'mabel ye _ still nineteen _ 1 - what you think of me', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 1 - what you think of me', howl: null },{ title: 'mabel ye _ still nineteen _ 10 - hometown', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 10 - hometown', howl: null },{ title: 'mabel ye _ still nineteen _ 11 - stuck at LAX', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 11 - stuck at LAX', howl: null },{ title: 'mabel ye _ still nineteen _ 12 - before leaving', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 12 - before leaving', howl: null },{ title: 'mabel ye _ still nineteen _ 2 - when your life is really hard', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 2 - when your life is really hard', howl: null },{ title: 'mabel ye _ still nineteen _ 3 - beautiful brain', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 3 - beautiful brain', howl: null },{ title: 'mabel ye _ still nineteen _ 4 - did it happen_', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 4 - did it happen_', howl: null },{ title: 'mabel ye _ still nineteen _ 5 - hunger, come to me!', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 5 - hunger, come to me!', howl: null },{ title: 'mabel ye _ still nineteen _ 6 - my bionicle boy', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 6 - my bionicle boy', howl: null },{ title: 'mabel ye _ still nineteen _ 7 - robot girl', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 7 - robot girl', howl: null },{ title: 'mabel ye _ still nineteen _ 8 - better without', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 8 - better without', howl: null },{ title: 'mabel ye _ still nineteen _ 9 - song of spite', file: '/mabel ye/still nineteen/mabel ye _ still nineteen _ 9 - song of spite', howl: null },{ title: 'Millie Manders _ Upside Down _ 1 - Upside Down', file: '/Millie Manders/Millie_Manders-Upside_Down-EP/Millie Manders _ Upside Down _ 1 - Upside Down', howl: null },{ title: 'Millie Manders _ Upside Down _ 2 - Nothing Matters', file: '/Millie Manders/Millie_Manders-Upside_Down-EP/Millie Manders _ Upside Down _ 2 - Nothing Matters', howl: null },{ title: 'Millie Manders _ Upside Down _ 3 - Paper Castles', file: '/Millie Manders/Millie_Manders-Upside_Down-EP/Millie Manders _ Upside Down _ 3 - Paper Castles', howl: null },{ title: 'Millie Manders _ Upside Down _ 4 - Monster', file: '/Millie Manders/Millie_Manders-Upside_Down-EP/Millie Manders _ Upside Down _ 4 - Monster', howl: null },{ title: 'Millie Manders and the Shutup _ Shutup _ 1 - Right to Life', file: '/Millie Manders/Millie_Manders_and_the_Shutup-Shutup-EP/Millie Manders and the Shutup _ Shutup _ 1 - Right to Life', howl: null },{ title: 'Millie Manders and the Shutup _ Shutup _ 2 - Brave', file: '/Millie Manders/Millie_Manders_and_the_Shutup-Shutup-EP/Millie Manders and the Shutup _ Shutup _ 2 - Brave', howl: null },{ title: 'Millie Manders and the Shutup _ Shutup _ 3 - Lollipops', file: '/Millie Manders/Millie_Manders_and_the_Shutup-Shutup-EP/Millie Manders and the Shutup _ Shutup _ 3 - Lollipops', howl: null },{ title: 'Millie Manders and the Shutup _ Shutup _ 4 - One That Got Away', file: '/Millie Manders/Millie_Manders_and_the_Shutup-Shutup-EP/Millie Manders and the Shutup _ Shutup _ 4 - One That Got Away', howl: null },{ title: 'mxmtoon _ dawn _ 1 - fever dream', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 1 - fever dream', howl: null },{ title: 'mxmtoon _ dawn _ 2 - used to you', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 2 - used to you', howl: null },{ title: 'mxmtoon _ dawn _ 3 - lessons', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 3 - lessons', howl: null },{ title: 'mxmtoon _ dawn _ 4 - quiet motions', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 4 - quiet motions', howl: null },{ title: 'mxmtoon _ dawn _ 5 - 1, 2', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 5 - 1, 2', howl: null },{ title: 'mxmtoon _ dawn _ 6 - no faker', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 6 - no faker', howl: null },{ title: 'mxmtoon _ dawn _ 7 - almost home', file: '/mxmtoon/dawn/mxmtoon _ dawn _ 7 - almost home', howl: null },{ title: 'mxmtoon _ dusk _ 1 - bon iver', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 1 - bon iver', howl: null },{ title: 'mxmtoon _ dusk _ 2 - ok on your own', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 2 - ok on your own', howl: null },{ title: 'mxmtoon _ dusk _ 3 - myrtle ave', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 3 - myrtle ave', howl: null },{ title: 'mxmtoon _ dusk _ 4 - wallflower', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 4 - wallflower', howl: null },{ title: 'mxmtoon _ dusk _ 5 - asking for a friend', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 5 - asking for a friend', howl: null },{ title: 'mxmtoon _ dusk _ 6 - show and tell', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 6 - show and tell', howl: null },{ title: 'mxmtoon _ dusk _ 7 - first', file: '/mxmtoon/dusk/mxmtoon _ dusk _ 7 - first', howl: null },{ title: 'mxmtoon _ plum blossom _ 1 - cliché', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 1 - cliche', howl: null },{ title: 'mxmtoon _ plum blossom _ 2 - i feel like chet', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 2 - i feel like chet', howl: null },{ title: 'mxmtoon _ plum blossom _ 3 - feelings are fatal', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 3 - feelings are fatal', howl: null },{ title: 'mxmtoon _ plum blossom _ 4 - the idea of you', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 4 - the idea of you', howl: null },{ title: 'mxmtoon _ plum blossom _ 5 - porcelain', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 5 - porcelain', howl: null },{ title: 'mxmtoon _ plum blossom _ 6 - temporary nothing', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 6 - temporary nothing', howl: null },{ title: 'mxmtoon _ plum blossom _ 7 - i miss you', file: '/mxmtoon/plum blossom/mxmtoon _ plum blossom _ 7 - i miss you', howl: null },{ title: 'mxmtoon _ the masquerade _ 1 - unspoken words (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 1 - unspoken words (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 1 - unspoken words', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 1 - unspoken words', howl: null },{ title: 'mxmtoon _ the masquerade _ 10 - late nights (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 10 - late nights (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 10 - late nights', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 10 - late nights', howl: null },{ title: 'mxmtoon _ the masquerade _ 2 - prom dress (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 2 - prom dress (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 2 - prom dress', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 2 - prom dress', howl: null },{ title: 'mxmtoon _ the masquerade _ 3 - suffice (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 3 - suffice (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 3 - suffice', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 3 - suffice', howl: null },{ title: 'mxmtoon _ the masquerade _ 4 - blame game (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 4 - blame game (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 4 - blame game', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 4 - blame game', howl: null },{ title: 'mxmtoon _ the masquerade _ 5 - high & dry (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 5 - high & dry (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 5 - high & dry', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 5 - high & dry', howl: null },{ title: 'mxmtoon _ the masquerade _ 6 - my ted talk (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 6 - my ted talk (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 6 - my ted talk', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 6 - my ted talk', howl: null },{ title: 'mxmtoon _ the masquerade _ 7 - seasonal depression (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 7 - seasonal depression (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 7 - seasonal depression', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 7 - seasonal depression', howl: null },{ title: 'mxmtoon _ the masquerade _ 8 - untitled (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 8 - untitled (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 8 - untitled', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 8 - untitled', howl: null },{ title: 'mxmtoon _ the masquerade _ 9 - dream of you (acoustic)', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 9 - dream of you (acoustic)', howl: null },{ title: 'mxmtoon _ the masquerade _ 9 - dream of you', file: '/mxmtoon/the masquerade/mxmtoon _ the masquerade _ 9 - dream of you', howl: null },{ title: 'mxmtoon feat Carly Rae Jepsen _ dawn & dusk _ 2 - ok on your own', file: '/mxmtoon feat. Carly Rae Jepsen/dawn & dusk/mxmtoon feat. Carly Rae Jepsen _ dawn & dusk _ 2 - ok on your own', howl: null },{ title: 'OH!hello _ BEACHY EP _ 1 - SHARKY', file: '/OH!hello/beachy-ep/OH!hello _ BEACHY EP _ 1 - SHARKY', howl: null },{ title: 'OH!hello _ BEACHY EP _ 2 - MY MANTA RAY', file: '/OH!hello/beachy-ep/OH!hello _ BEACHY EP _ 2 - MY MANTA RAY', howl: null },{ title: 'OH!hello _ BEACHY EP _ 3 - BEACH HOUSE', file: '/OH!hello/beachy-ep/OH!hello _ BEACHY EP _ 3 - BEACH HOUSE', howl: null },{ title: 'OH!hello _ BEACHY EP _ 4 - GLEN & EDEN', file: '/OH!hello/beachy-ep/OH!hello _ BEACHY EP _ 4 - GLEN & EDEN', howl: null },{ title: 'OH!hello _ BEACHY EP _ 5 - ALGAE', file: '/OH!hello/beachy-ep/OH!hello _ BEACHY EP _ 5 - ALGAE', howl: null },{ title: 'OH!hello _ RIP TIDES _ 1 - LAVA POOLS', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 1 - LAVA POOLS', howl: null },{ title: 'OH!hello _ RIP TIDES _ 2 - BLACK DENIM', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 2 - BLACK DENIM', howl: null },{ title: 'OH!hello _ RIP TIDES _ 3 - MURDA', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 3 - MURDA', howl: null },{ title: 'OH!hello _ RIP TIDES _ 4 - SMMR SDNSS', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 4 - SMMR SDNSS', howl: null },{ title: 'OH!hello _ RIP TIDES _ 5 - CAVES & WAVES', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 5 - CAVES & WAVES', howl: null },{ title: 'OH!hello _ RIP TIDES _ 6 - TANGERINE', file: '/OH!hello/rip-tides/OH!hello _ RIP TIDES _ 6 - TANGERINE', howl: null },{ title: 'OH!hello _ Sleepy _ 1 - Overture', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 1 - Overture', howl: null },{ title: 'OH!hello _ Sleepy _ 10 - Flowerhead', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 10 - Flowerhead', howl: null },{ title: 'OH!hello _ Sleepy _ 11 - Slater', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 11 - Slater', howl: null },{ title: 'OH!hello _ Sleepy _ 2 - Coffee & Toast', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 2 - Coffee & Toast', howl: null },{ title: 'OH!hello _ Sleepy _ 3 - Paris Song', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 3 - Paris Song', howl: null },{ title: 'OH!hello _ Sleepy _ 4 - Savana Sabertooth', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 4 - Savana Sabertooth', howl: null },{ title: 'OH!hello _ Sleepy _ 5 - Nocturnes', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 5 - Nocturnes', howl: null },{ title: 'OH!hello _ Sleepy _ 6 - The Cold', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 6 - The Cold', howl: null },{ title: 'OH!hello _ Sleepy _ 7 - Zzzz', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 7 - Zzzz', howl: null },{ title: 'OH!hello _ Sleepy _ 8 - Tiger Lily', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 8 - Tiger Lily', howl: null },{ title: 'OH!hello _ Sleepy _ 9 - Despondency', file: '/OH!hello/sleepy/OH!hello _ Sleepy _ 9 - Despondency', howl: null },{ title: 'OH!hello _ WILDERNESS _ 1 - Tastes', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 1 - Tastes', howl: null },{ title: 'OH!hello _ WILDERNESS _ 10 - Sail Song', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 10 - Sail Song', howl: null },{ title: 'OH!hello _ WILDERNESS _ 11 - Walk You Home', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 11 - Walk You Home', howl: null },{ title: 'OH!hello _ WILDERNESS _ 2 - Amore', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 2 - Amore', howl: null },{ title: 'OH!hello _ WILDERNESS _ 3 - Teepee', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 3 - Teepee', howl: null },{ title: 'OH!hello _ WILDERNESS _ 4 - Wilderness', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 4 - Wilderness', howl: null },{ title: 'OH!hello _ WILDERNESS _ 5 - I Am a Fox', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 5 - I Am a Fox', howl: null },{ title: 'OH!hello _ WILDERNESS _ 6 - Leaving Home', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 6 - Leaving Home', howl: null },{ title: 'OH!hello _ WILDERNESS _ 7 - Big Shirt', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 7 - Big Shirt', howl: null },{ title: 'OH!hello _ WILDERNESS _ 8 - Over', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 8 - Over', howl: null },{ title: 'OH!hello _ WILDERNESS _ 9 - Pt Deux', file: '/OH!hello/wilderness/OH!hello _ WILDERNESS _ 9 - Pt. Deux', howl: null },{ title: 'The Sonder Bombs _ The Sonder Bombs _ 1 - Nonsense', file: '/The Sonder Bombs/The Sonder Bombs EP/The Sonder Bombs _ The Sonder Bombs _ 1 - Nonsense', howl: null },{ title: 'The Sonder Bombs _ The Sonder Bombs _ 2 - End of My Daze', file: '/The Sonder Bombs/The Sonder Bombs EP/The Sonder Bombs _ The Sonder Bombs _ 2 - End of My Daze', howl: null },{ title: 'The Sonder Bombs _ The Sonder Bombs _ 3 - Doubts', file: '/The Sonder Bombs/The Sonder Bombs EP/The Sonder Bombs _ The Sonder Bombs _ 3 - Doubts', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 1 - Kawika', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 1 - Kawika', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 10 - Poli Pumehana', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 10 - Poli Pumehana', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 11 - Guava Jam', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 11 - Guava Jam', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 2 - Only You', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 2 - Only You', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 3 - Heha Waipi\'o', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 3 - Heha Waipi\'o', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 4 - Kaulana \'o Waimanalo', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 4 - Kaulana \'o Waimanalo', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 5 - Ka\' Ililauokekoa', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 5 - Ka\' Ililauokekoa', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 6 - Mehameha', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 6 - Mehameha', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 7 - Hawai\'i Au', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 7 - Hawai\'i Au', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 8 - Maika\'i Ka Makani O Kohala', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 8 - Maika\'i Ka Makani O Kohala', howl: null },{ title: 'The Sunday Manoa _ Guava Jam _ 9 - Ka La\'i \'Opua', file: '/The Sunday Manoa/Guava Jam/The Sunday Manoa _ Guava Jam _ 9 - Ka La\'i \'Opua', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 1 - For You', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 1 - For You', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 10 - Fiya', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 10 - Fiya', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 11 - Synonynonym', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 11 - Synonynonym', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 12 - Want Me To', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 12 - Want Me To', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 13 - Real Live Flesh', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 13 - Real Live Flesh', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 2 - Sunlight', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 2 - Sunlight', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 3 - Lions', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 3 - Lions', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 4 - Hatari', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 4 - Hatari', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 5 - News', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 5 - News', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 6 - Jamaican', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 6 - Jamaican', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 7 - Jumping Jack', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 7 - Jumping Jack', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 8 - Little Tiger', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 8 - Little Tiger', howl: null },{ title: 'tUnE-yArDs _ BiRd-BrAiNs _ 9 - Safety', file: '/tUnE-yArDs/BiRd-BrAiNs/tUnE-yArDs _ BiRd-BrAiNs _ 9 - Safety', howl: null }
]);

// Bind our player controls.
playBtn.addEventListener('click', function() {
  player.play();
});
pauseBtn.addEventListener('click', function() {
  player.pause();
});
prevBtn.addEventListener('click', function() {
  player.skip('prev');
});
nextBtn.addEventListener('click', function() {
  player.skip('next');
});
waveform.addEventListener('click', function(event) {
  player.seek(event.clientX / window.innerWidth);
});
playlistBtn.addEventListener('click', function() {
  player.togglePlaylist();
});
playlist.addEventListener('click', function() {
  player.togglePlaylist();
});
volumeBtn.addEventListener('click', function() {
  player.toggleVolume();
});
volume.addEventListener('click', function() {
  player.toggleVolume();
});

// Setup the event listeners to enable dragging of volume slider.
barEmpty.addEventListener('click', function(event) {
  var per = event.layerX / parseFloat(barEmpty.scrollWidth);
  player.volume(per);
});
sliderBtn.addEventListener('mousedown', function() {
  window.sliderDown = true;
});
sliderBtn.addEventListener('touchstart', function() {
  window.sliderDown = true;
});
volume.addEventListener('mouseup', function() {
  window.sliderDown = false;
});
volume.addEventListener('touchend', function() {
  window.sliderDown = false;
});

var move = function(event) {
  if (window.sliderDown) {
    var x = event.clientX || event.touches[0].clientX;
    var startX = window.innerWidth * 0.05;
    var layerX = x - startX;
    var per = Math.min(1, Math.max(0, layerX / parseFloat(barEmpty.scrollWidth)));
    player.volume(per);
  }
};

volume.addEventListener('mousemove', move);
volume.addEventListener('touchmove', move);

// Setup the "waveform" animation.
var wave = new SiriWave({
  container: waveform,
  width: window.innerWidth,
  height: window.innerHeight * 0.3,
  cover: true,
  speed: 0.03,
  amplitude: 0.7,
  frequency: 2
});
wave.start();

// Update the height of the wave animation.
// These are basically some hacks to get SiriWave.js to do what we want.
var resize = function() {
  var height = window.innerHeight * 0.3;
  var width = window.innerWidth;
  wave.height = height;
  wave.height_2 = height / 2;
  wave.MAX = wave.height_2 - 4;
  wave.width = width;
  wave.width_2 = width / 2;
  wave.width_4 = width / 4;
  wave.canvas.height = height;
  wave.canvas.width = width;
  wave.container.style.margin = -(height / 2) + 'px auto';

  // Update the position of the slider.
  var sound = player.playlist[player.index].howl;
  if (sound) {
    var vol = sound.volume();
    var barWidth = (vol * 0.9);
    sliderBtn.style.left = (window.innerWidth * barWidth + window.innerWidth * 0.05 - 25) + 'px';
  }
};
window.addEventListener('resize', resize);
resize();
